const keys = (obj) => {
    if(!obj){
        return [];
    }

    let result = [];

    for (let key in obj) {
        result.push(key);
    }
    return result;
};

module.exports = keys;