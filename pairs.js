const pairs = (obj) => {
    if (!obj) {
        return [];
    }

    let out = [];

    for (let i in obj) {
        out.push([i, obj[i]]);

    } 
    return out;

};


module.exports = pairs;