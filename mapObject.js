const mapObject = (obj, cb) => {
    
    if(!obj || typeof cb !== 'function'){
        return {};
    }

    for(let key in obj){
        let a = cb(obj[key],[key]);
            obj[key] = a;
        }
    return obj;
};


module.exports = mapObject;