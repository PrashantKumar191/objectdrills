const defaults = (obj, defaultProps) => {

    if(!obj){
        return {};
    }
    else{

    for(let key in obj){
        if(key in defaultProps){
            obj[key] = defaultProps[key];
            }
    }
    return obj;
    }
};

module.exports = defaults;