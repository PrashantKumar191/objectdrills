const defaultsFunction = require('../defaults.js')



const testObject = { name: 'Bruce Wayne', age: 36, myfun() { console.log('hello') }, location: 'Gotham' }; 
const defaultProps = {name: 'John', age :32, car : 'Audi' };

console.log(defaultsFunction(testObject,defaultProps));

defaultsFunction();
defaultsFunction({});
defaultsFunction([]);
defaultsFunction(null);