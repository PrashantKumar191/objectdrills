const invert = (obj) => {
    if(!obj){
        return {};
    }
    let out = {};

    for(let key in obj){
        out[obj[key]] = key;
    }
    return out;
};
    
module.exports = invert;