const values = (obj) => {
    if(!obj){
        return {};
    }
    
    let result = [];
    for(let i in obj){
        if(typeof(obj[i]) !== 'function'){
            result.push(obj[i]);
        }
    }
    return result;
};


module.exports = values;